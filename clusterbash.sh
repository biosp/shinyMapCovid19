#!/bin/bash
#
#SBATCH -J ShinyCovid19
#SBATCH -o logshinycovid19_%J_%2t.out
#SBATCH -e errorshinycovid19_%J_%2t.out
#
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=3
#SBATCH --time=08:10:00
#SBATCH --mem-per-cpu=4092
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jean-francois.rey@inrae.fr

module purge
module load R/4.0.0

srun -n1 -N1 -o logshinycovid19_%J.out -e errorshinycovid19_%J.out Rscript ./main_forecast_plotly.R "0" &
srun -n1 -N1 -o logshinycovid19_%J.out -e errorshinycovid19_%J.out Rscript ./main_forecast_plotly.R "1" &
srun -n1 -N1 -o logshinycovid19_%J.out -e errorshinycovid19_%J.out Rscript ./main_forecast_plotly.R "2" &
srun -n1 -N1 -o logshinycovid19_%J.out -e errorshinycovid19_%J.out Rscript ./main_forecast_plotly.R "3" &


echo "# on attend les 4 builds..."
wait
echo "# 4 builds fini ! on merge..."

srun Rscript ./main_forecast_plotly.R "merge"
echo "# Merge fini"
wait
echo "# Trigger pipeline "
curl --request POST --form token=TOKEN --form ref=master https://gitlab.paca.inrae.fr/api/v4/projects/105/trigger/pipeline
echo "# Done!"
