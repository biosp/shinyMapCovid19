# Shiny App Map Covid19

## DATA
* **Forecast**  
    Johns Hopkins University Center for Systems Science and Engineering (JHU CSSE)  
    See their github repository: https://github.com/CSSEGISandData/COVID-19

## Authors

* Virgile Baudrot
* Mélina Ribaud
* Jean-François Rey

## Inline Shiny App

[Online Map Covid19 App](https://shiny.biosp.inrae.fr/app_direct/mapCovid19/)

## Run locally Docker App image

Get and execute localy the docker image of the app.

```
sudo docker run -p 3838:3838 --name mapcovid19 gitlab.paca.inra.fr:4567/biosp/shinymapcovid19:latest 
```
The application is accessible at : [http://localhost:3838](http://localhost:3838)  

## Regenerate Docker image

### Update Forecast data

To update forecast data :
```
cd R
Rscript main_forecast_plotly.R "0"
Rscript main_forecast_plotly.R "1"
Rscript main_forecast_plotly.R "2"
Rscript main_forecast_plotly.R "3"
Rscript main_forecast_plotly.R "merge"
mv R/list_figures.rds data/list_figures.rds
```

### Build Docker image

```
sudo docker build --build-arg APP_NAME=ShinyMapCovid19 --cache-from gitlab.paca.inra.fr:4567/biosp/shinymapcovid19:latest --tag shinymapcovid19:latest -f Dockerfile.multi .
```

With folder locally

```
sudo docker build --build-arg APP_NAME=ShinyMapCovid19 --tag shinymapcovid19:latest -f Dockerfile.multi .
```

### Run locally

```
sudo docker run -p 3838:3838 --name mapcovid19 shinymapcovid19:latest 
```

Go to : [http://localhost:3838/](http://localhost:3838)

To stop and remove container
```
sudo docker stop mapcovid19
sudo docker container rm mapcovid19
```
