#!/usr/bin/env Rscript

## List all files
files <- list.files(path="/mnt/biomstock/Cluster/biometrie/parsi_covid/SUIVI_COVID/COVIDM3", pattern='immun_M3_k[0-9]+.txt', full.names= TRUE)

## order files by week
order_files <- files[order(nchar(files),files)]

first_date <- as.Date("2020-03-30")

## read all file contain
read_files <- lapply(order_files, FUN = function(f) {
                                  
                                   return( as.numeric(read.csv(f, header=FALSE, sep = ",", dec = ".")))
                                })

## add date as list names (index)
lapply(seq_along(read_files), function(i) { names(read_files)[[i]] <<- as.character(first_date + 7*i)})

## save it 
saveRDS(read_files, "immun_M3_k.rds")


